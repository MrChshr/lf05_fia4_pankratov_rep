﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;

       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(rückgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    
   //to get a price for one ticket
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag; 
        
      //to get a price for one ticket
    	System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        
      //Anzahl der Fahrkarten geben
        int tickets = 0;
        System.out.print("Anzahl der Tickets: ");
        tickets = tastatur.nextInt();
        zuZahlenderBetrag = zuZahlenderBetrag * tickets;
        
    	return zuZahlenderBetrag;
    }
    
    //receive a paid sum and return a return amount
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.print("Noch zu zahlen: ");
     	   //Aufgabe 7
     	   System.out.printf("%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void warte(int millisekunde) {
    	try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	//to avoid the inaccuracy because of double-type
        rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
        
        if(rückgabebetrag >  0.0)
        {
     	   System.out.print("Der Rückgabebetrag in Höhe von ");
     	   
     	   
     	   System.out.printf("%.2f", rückgabebetrag);
     	   
     	   System.out.println(" EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
  	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
 	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT ");
  	          rückgabebetrag -= 0.05;
  	          rückgabebetrag = Math.round(rückgabebetrag * 100.0)/100.0;
            }
        }
    }
}